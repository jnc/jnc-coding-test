# jnc-coding-test

Create a Liquor deal finding app, using the free LCBOApi (https://lcboapi.com/docs/). You will need to get an API key. You can use any approach and technology you prefer: SPA, MVC web app, etc.

It should do the following:

* Return a list of alcohol products that are on sale (has_limited_time_offer). 
* Display the list with relevant details on the webpage.
* Be sortable by Current Price or Discount Amount.
* Be filterable by Tags (This is not available in the API and will need to be implemented yourself)

Spend no more than 3 hours on this. Use git to manage your commits, and push the code up to github or bitbucket and send the repo to JNC. If you have any questions contact {Jeremy/Sean/Development}